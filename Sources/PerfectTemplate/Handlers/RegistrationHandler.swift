//
//  RegistrationHandler.swift
//  COpenSSL
//
//  Created by Дмитрий on 29/04/2019.
//

import PerfectHTTP

public class RegistrationHandler: AbstractHandler{
    
    public func process(request: HTTPRequest, response: HTTPResponse) {
        let login = request.param(name: "login")
        let password = request.param(name: "password")
        
        response.setHeader(.contentType, value: "text/html")
        
       
        response.appendBody(string: "{ \"result\": 1,\"userMessage\": \"Регистрация прошла успешно!\" }")
        
        /*response.appendBody(string: "{\"user\":{\"login\":\"\(login ?? "nil")\", \"password\":\"\(password ?? "nil")\"}}")*/
        response.completed()
    }
}

