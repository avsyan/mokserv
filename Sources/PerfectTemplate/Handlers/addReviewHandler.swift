//
//  addReviewHandler.swift
//  COpenSSL
//
//  Created by Арсений Дорогин on 13/05/2019.
//

import PerfectHTTP

public class addReviewHandler: AbstractHandler{
    
    public func process(request: HTTPRequest, response: HTTPResponse) {
        let idUser = request.param(name: "id_user")
        let text = request.param(name: "text")
        
        response.setHeader(.contentType, value: "text/html")
        
        
        response.appendBody(string: "{ \"result\": 1,\"userMessage\": \"Ваш отзыв был передан в модерацию\" }")
        
        response.completed()
    }
}
