//
//  CatalogDataHandler.swift
//  COpenSSL
//
//  Created by Арсений Дорогин on 13/05/2019.
//

import PerfectHTTP

public class CatalogDataHandler: AbstractHandler{
    
    public func process(request: HTTPRequest, response: HTTPResponse) {
        let pageNumber = request.param(name: "page_number")
        let idCategory = request.param(name: "id_category")
        
        response.setHeader(.contentType, value: "text/html")
        
      
        response.appendBody(string: "[{\"id_product\": 123,\"product_name\": \"Ноутбук\",\"price\": 45600},{\"id_product\": 456,\"product_name\": \"Мышка\",\"price\": 1000}]")
        
        response.completed()
    }
}
