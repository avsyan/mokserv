//
//  Logout.swift
//  COpenSSL
//
//  Created by Арсений Дорогин on 13/05/2019.
//

import PerfectHTTP

public class LogoutHandler: AbstractHandler{
    
    public func process(request: HTTPRequest, response: HTTPResponse) {
        let idUser = request.param(name: "id_user")
        
        response.setHeader(.contentType, value: "text/html")
        
        
        response.appendBody(string:"{\"result\": 1}")
        response.completed()
    }
}

