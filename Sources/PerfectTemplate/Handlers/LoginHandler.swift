//
//  LoginHandler.swift
//  COpenSSL
//
//  Created by Арсений Дорогин on 13/05/2019.
//

import PerfectHTTP

public class LoginHandler: AbstractHandler{
    
    public func process(request: HTTPRequest, response: HTTPResponse) {
        let login = request.param(name: "login")
        let password = request.param(name: "password")
        
        response.setHeader(.contentType, value: "text/html")
        
        
        //response.appendBody(string: "{ \"result\": 1,\"userMessage\": \"Регистрация прошла успешно!\" }")
        response.appendBody(string:
            "{\"result\": 1,\"user\": { \"id_user\": 123,\"user_login\": \"\(login ?? "nil")\", \"user_name\": \"John\",\"user_lastname\": \"Doe\"}}"
            )
        /*
        response.appendBody(string: "{\"result\": 1,\"user\":{\"login\":\"\(login ?? "nil")\", \"password\":\"\(password ?? "nil")\"}}")*/
        response.completed()
    }
}

