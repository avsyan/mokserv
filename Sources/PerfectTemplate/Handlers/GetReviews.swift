//
//  GerReviews.swift
//  COpenSSL
//
//  Created by Арсений Дорогин on 13/05/2019.
//

import PerfectHTTP

public class GetReviewsHandler: AbstractHandler{
    
    public func process(request: HTTPRequest, response: HTTPResponse) {
        let idProduct = request.param(name: "id_product")
        
        response.setHeader(.contentType, value: "text/html")
        
        if idProduct == "123"{
            response.appendBody(string: "[{\"id_review\": 1,\"id_user\": 123,\"text\": \"Отличный компьютер\"},{\"id_review\": 2,\"id_user\": 256,\"text\": \"Сломался через неделю после покупки\"}]")
        }
        if idProduct == "456"{
            response.appendBody(string: "[{\"id_review\": 3,\"id_user\": 123,\"text\": \"Классная мышка\"},{\"id_review\": 4,\"id_user\": 256,\"text\": \"Отлично подходит для игр в компьютер\"}]")
        }
        
        response.completed()
    }
}
