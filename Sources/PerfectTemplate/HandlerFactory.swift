//
//  HandlerFactory.swift
//  COpenSSL
//
//  Created by Дмитрий on 29/04/2019.
//

import PerfectHTTP

public class HandlerFactory {
    
    public func handlerFor(request: HTTPRequest, response: HTTPResponse) -> AbstractHandler{
        switch request.path {
        case "/registration":
            return RegistrationHandler()
            
        case "/login":
            return LoginHandler()
        case "/logout":
            return LogoutHandler()
        case "/addReview":
            return addReviewHandler()
        case "/catalogData":
            return CatalogDataHandler()
        case "/getReviews":
            return GetReviewsHandler()
        default:
            return ErrorHandler()
        }
        
    }
}
